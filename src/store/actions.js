export const ENTER = 'ENTER';
export const CALC = 'CALC';
export const RESULT = 'RESULT';
export const CLEAR = 'CLEAR';
export const BACK = 'BACK';

export const buttonHandler = (value) => {
	return (dispatch) => {
		dispatch({type: ENTER, value});
	}
};

export const getCalc = mark => {
	return (dispatch, getState) => {
		if (!getState().block) {
			dispatch({type: CALC, mark});
		}
	}
};

export const result = () => {
	return (dispatch, getState) => {
		let last = getState().inputValue.toString().slice(-1);
		if (parseInt(last,10) || last === '0') {
			dispatch({type: RESULT});
		}
	}
};

export const clear = () => {
	return (dispatch) => {
		dispatch({type: CLEAR});
	}
};

export const back = () => {
	return (dispatch) => {
		dispatch({type: BACK});
	}
};