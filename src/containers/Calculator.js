import React from 'react';
import {StyleSheet, Button, TextInput, View, Text} from 'react-native';
import Keyboard from "../../src/components/Keyboard/Keyboard";
import {connect} from 'react-redux'
class Calculator extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inputContainer}>
					<Text style={styles.input}>{this.props.value}</Text>
				</View>
				<View style={styles.placesContainer}>
					<Keyboard/>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		value: state.inputValue
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		paddingTop: 40,
		alignItems: 'center',
		justifyContent: 'flex-start',
		backgroundColor: '#000'
	},
	inputContainer: {
		flex: 0,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		borderWidth: 2,
		borderColor: '#000',
		backgroundColor: '#fff',
		width: '100%'
	},
	input: {
		width: '100%',
		paddingRight: 20,
		paddingLeft: 20,
		height: 80,
		lineHeight: 80,
		fontSize: 40,
		fontWeight: 'bold',
		textAlign: 'right'
	},
	placesContainer: {
		flex: 1,
		flexDirection: 'column',
		width: '100%'
	}
});

export default connect(mapStateToProps)(Calculator);