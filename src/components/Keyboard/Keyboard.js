import React from 'react'
import {Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux'
import {back, buttonHandler, clear, getCalc, result} from "../../store/actions";

class Keyboard extends React.Component {
	render() {
		return (
			<View style={styles.keysContainer}>
				<View style={styles.buttonContainer}>
					{
						this.props.digits.map(digit => (
							<TouchableOpacity
								onPress={() => this.props.buttonHandler(digit)}
								key={digit}
								style={styles.button}>
								<View>
									<Text style={styles.buttonText}>{digit}</Text>
								</View>
							</TouchableOpacity>
						))
					}
				</View>
				<View style={styles.buttonContainer}>
					{
						this.props.buttons.map(btn => (
							<TouchableOpacity
								onPress={() => this.props.getCalc(btn)}
								key={btn}
								style={styles.actionButtons}>
								<View>
									<Text style={styles.buttonText}>{btn}</Text>
								</View>
							</TouchableOpacity>
						))
					}
					<TouchableOpacity
						onPress={this.props.result}
						style={styles.actionButtons}>
						<View>
							<Text style={styles.buttonText}>=</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={this.props.clear}
						style={styles.actionButtons}>
						<View>
							<Text style={styles.buttonText}>Clear</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={this.props.back}
						style={styles.actionButtons}>
						<View>
							<Text style={styles.buttonText}>Back</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	keysContainer: {
		flex: 1,
		flexDirection: 'column',
		width: '100%',
		backgroundColor: '#fff'
	},
	buttonContainer: {
		flex: 1,
		flexWrap: 'wrap',
		padding: 10,
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		backgroundColor: '#fff',
	},
	actionButtons: {
		width: '31%',
		height: '31%',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'blue',
		borderRadius: 4,
		marginBottom: 10
	},
	button: {
		width: '31%',
		height: '31%',
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 10,
		backgroundColor: '#ccc',
		borderRadius: 4,
	},
	buttonText: {
		color: '#fff',
		fontSize: 24,
		fontWeight: 'bold',
	}
});

const mapStateToProps = state => {
	return {
		digits: state.digits,
		buttons: state.buttons
	}
};

const mapDispatchToProps = dispatch => {
	return {
		buttonHandler: (value) => dispatch(buttonHandler(value)),
		getCalc: (mark) => dispatch(getCalc(mark)),
		result: () => dispatch(result()),
		clear: () => dispatch(clear()),
		back: () => dispatch(back())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Keyboard);


