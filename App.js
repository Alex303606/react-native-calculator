import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Calculator from "./src/containers/Calculator";
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux'
import reducer from './src/store/reducer'
import thunkMiddleware from 'redux-thunk'
const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<Provider store={store}>
					<Calculator/>
				</Provider>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
